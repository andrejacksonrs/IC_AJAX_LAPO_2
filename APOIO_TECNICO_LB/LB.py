""" 
Reproduzindo os resultados de Lucas Barbosa em Python
Parte 1 - Gráfico de Barras

Autor: André Jackson Ramos Simões
Baseado em script para MATLAB desenvolvido por Lucas Barbosa e Johnatan Dantas 

ATENÇÃO !! AO EXECUTAR O CÓDIGO MAIS DE UMA VEZ, LIMPAR AS VARIÁVEIS ANTES, RESETANDO O CONSOLE POR EXEMPLO.

"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob

#Lendo os nomes dos arquivos do diretório desejado
pastas = ["Inverno","Outono", "Primavera", "Verão"]

# Número da pasta que iremos acessar e fazer o gráfico 
n = 0

pasta = pastas[n]

files = glob.glob(pasta + "/**")

lp = len(pasta)
lf = len(files)

for i in range(len(files)):
    lfi = len(files[i])
    b = files[i][lp+1:lfi-4]
    files[i] = b


#Inicializando os dataframes com os dados dos arquivos .txt (tn := tabela do tipo numpy, tp:= tabela do tipo dataframe)
tn = np.array([[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17]])
tp = pd.DataFrame(tn)

#Lendo os arquivos e criando uma tabela contendo todos os dados dos .txt

for i in range(0,lf):
    
    t = pd.read_csv(pasta + "/" + files[i] + ".txt" , sep = ";")
    t = t.drop(columns = 'str')
    t = t.rename(columns ={'str' : 'Canal', 'media2' : files[i]})
    
    tp = tp.join(t)
    tn = np.append(tn, t, axis = 1)

#Calculando os valores médios de cada linha

medias = np.array([])

for i in range(0,17):
    
    m = np.mean(tn[i,1:])
    medias = np.append(medias,m)





#plot dos pontos

x = np.linspace(1,17,17)
y = medias
#y = medias/(max(medias))

plt.bar(x,y, color = 'orange')


#Títulos, legendas e salvando a figura
#plt.legend(loc='best')


plt.xticks(x)
#plt.yticks(np.linspace(0,2000,5))
plt.ylim(0,2000)
plt.xlabel('Canal', size = 10)

plt.ylabel('Intensidade Luminosa (Contagens de ADC)', size = 10)
#plt.ylabel('ADC (Normalizada pelo valor máximo)', size = 10)

plt.title(pasta, size = 15)

plt.savefig(pasta + '.png', dpi = 300)
