"""

Fitting - Ajuste de curvas

Autor: André Jackson Ramos Simões

"""
#Bibliotecas usadas
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize

#Dados
x = np.array([1,2,3,4,5])
y = np.array([1,4.2,8.5,17,24])


#------------------------------------------------------------------------------
#Realizando o Fitting
def f(x,a,b):
    return a*x + b
    #return a*pow(x,b) + c


guess = [1,1]

params, params_covariance = scipy.optimize.curve_fit(f, x, y, guess)
params

a = params[0]
b = params[1]
#c = params[2]

#Imprimindo os coeficientes ajustados
print("Coeficiente a:")
print(a)
print("Coeficiente b:")
print(b)
#print("Coeficiente c:")
#print(c)

#------------------------------------------------------------------------------


#plot dos pontos
plt.scatter(x,y, color = 'black', label = 'Dados Experimentais')

#plot da curva ajustada
x = np.linspace(1.0,5.0,100)
plt.plot(x, f(x,a,b), color="blue", label = 'Curva ajustada')


#Títulos, legendas e salvando a figura
plt.legend(loc='best')

plt.xlabel('nome do eixo x', size = 10)

plt.ylabel('nome do eixo y', size = 10)

plt.title('Título do Gráfico', size = 15)

#plt.savefig('pontos.png', dpi = 300)









