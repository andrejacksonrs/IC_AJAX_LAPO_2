""" 
Importando e plotando pontos de um arquivo .txt

Autor: André Jackson Ramos Simões
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#Lendo os dados do arquivo .txt localizado na mesma pasta deste arquivo .py
tabela = pd.read_csv("seno.txt" , sep = ";", header = 0).to_numpy()

#definindo as variáveis x e y que serão plotadas
x = tabela[:,0]
y = tabela[:,1]

#plot dos pontos
plt.scatter(x,y, color = 'black', label = 'Legenda')


#Títulos, legendas e salvando a figura
plt.legend(loc='best')

plt.xlabel('nome do eixo x', size = 10)

plt.ylabel('nome do eixo y', size = 10)

plt.title('Título do Gráfico', size = 15)

plt.savefig('nomedografico.png', dpi = 300)































