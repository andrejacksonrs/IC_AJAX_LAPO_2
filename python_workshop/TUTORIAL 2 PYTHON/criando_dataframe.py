""" 
Criando um dataframe com python

Autor: André Jackson Ramos Simões
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#Escrevendo os dados
x = np.array([1,2,3,4,5])

y = np.array([1,2,3,4,5])

#Tabela

#Criação da Tabela
tabela = pd.DataFrame({'nome da coluna 0': x, 'nome da coluna 1': y})

#Exportação da tabela para um arquivo .txt
tabela.to_csv('dataframe.txt',index = False, sep = ';')

#BÔNUS: Exportação da tabela para latex
#tabela.to_latex('tabelalatex.txt',index = False)