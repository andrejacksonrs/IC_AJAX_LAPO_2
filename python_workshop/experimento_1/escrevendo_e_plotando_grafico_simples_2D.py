""" 
Escrevendo e Plotando um Gráfico 2D

Autor: André Jackson Ramos Simões
"""

import numpy as np
import matplotlib.pyplot as plt

#Escrevendo os dados
x = np.array([1,2,3,4,5])

y = np.array([5,4,3,2,1])

#Plotando os dados
plt.scatter(x,y, color = 'black', label = 'nome dos pontos')

#Títulos, Legendas e salvando o gráfico
plt.legend(loc='best')

plt.xlabel('Nome do eixo x', size = 10)

plt.ylabel('Nome do eixo y', size = 10)

plt.title('Título do gráfico', size = 15)

#plt.savefig('nomedografico.png', dpi = 300)
