# [1] Solar Radiation Theory

# [2] Illumination Engineering

## Definição de grandezas

Trecho retirado do texto, pg. 4 (pg. 22 do pdf), seção 1.3 Units

Radiometric Terms. Deterministic quantities based on the physical nature of
light. These terms are typically used in nonvisual systems; and
• Photometric Terms. Quantities based on the human visual system such that
only visible electromagnetic radiation is considered. This system of units is
typically used in visual systems.

Grandezas radiométricas:

![image-20211021222931570](C:\Users\andre\AppData\Roaming\Typora\typora-user-images\image-20211021222931570.png)

Tabela 1.1, localizada na página 5 do livro, capítulo 1, página 22 do pdf do livro.



Grandezas fotométricas:

![image-20211021222613763](C:\Users\andre\AppData\Roaming\Typora\typora-user-images\image-20211021222613763.png)

Tabela 1.2 localizada na página 6 do livro, capítulo 1, página 23 do pdf do livro.

# [3] Insect Eyes Inspire Improved Solar Cells

**ESPECTRO SOLAR**: The wavelengths in the solar spectrum range from 250 to 3,000 nm, with peak intensity at about 550 nm. For exploiting the photovoltaic effect using silicon, the 400 to 1,100 nm wavelength range must be considered. Despite the evolution of silicon photovoltaic technology for more than half a century, the efficiency of silicon solar cells remains rather low. (pg. 40 do artigo, 41 do .pdf, na parte de baixo da página logo no começo do tópico "Photovoltaics Today")

**Engineered Biomimicry**

- Bioinspiration
- Biomimetics
- Bioreplication

Tudo isso foi abordado na página 42 do pdf e 41 do volume da revista.

"Results indicated that the bioinspired textured solar cell
exhibits light-coupling effi ciency—i.e., the solar light transmitted
into silicon with respect to the total amount of light
impinging on the exposed surface, averaged over the maximal
angular fi eld of view and the solar spectrum—that is signifi -
cantly superior to the commonly used textured and untextured
silicon solar cells. Th is effi ciency is comparable to that of antirefl
ection-(AR)-coated untextured silicon solar cells."

"In order to improve performance further, the bioinspired prismatically textured surface can be coated with an AR coating. Then the light-coupling efficiency can be enhanced by as much as 93 percent with respect to that of the bare untextured silicon surface, by about 35 percent with respect to the AR-coated untextured surface, and by about 60 percent with respect to the groove-textured surface." - Trecho retirado da página 43 do .pdf, pg. 42 da revista, no antepenultimo parágrafo da página.



# Referências Bibliográficas

Modelo para Artigo em periódicos conforme manual do estilo acadêmico da UFBA (pg. 109, 111 do pdf)

SOBRENOME, Prenome [abreviado ou não]. Título do artigo: subtítulo. **Título da**
**publicação**, local, número do volume e/ou ano, número do fascículo, página inicial e
final do artigo, informações do período e data de publicação.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Monografia no todo: Livros, Trabalho Acadêmico, Folhetos,
Manuais, Guias, Catálogos, Enciclopédias, Dicionários etc
Sequência dos elementos essenciais:

SOBRENOME, Prenome [abreviado ou não]. **Título**: subtítulo. Edição [registrar da
segunda em diante]. Local: Editora, data. nº de páginas ou volumes. (Série, nº ou v.)

Obs.: Quando houver quatro ou mais autores, convém indicar todos. No entanto, é
facultada a indicação apenas do primeiro seguido da expressão *et al*. (pg. 107 do .pdf do manual do estilo acadêmico da UFBA)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

[1] WIDÉN, J.; MUNKHAMMAR, J. **Solar Radiation Theory**. Sweden: Uppsala University, 2019. 50p. ISBN 978-91-506-2760-2. DOI 10.33063/diva-381852

[2] KOSHEL, R. **Illumination Engineering**: Design with Nonimagin Optics. Tucson, Arizona, College of Optical Sciences, The University of Arizona: John Wiley & Sons Inc, 2013. 302 p. ISBN 978-0-470-91140-2.

[3] CHIADINI, F. *et al*. Insect Eyes Inspire Improved Solar Cells. **Optics and Photonics News**, v. 22, p. 38-43, 2011. DOI: 10.1364/OPN.22.4.000038.